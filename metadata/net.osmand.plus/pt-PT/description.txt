OsmAnd+ (OSM Automated Navigation Directions - Instruções de Navegação Automatizada do OSM) é uma aplicação de mapas e navegação com acesso livre aos dados do OpenStreetMap (OSM). Desfrute da navegação por voz ou visual, consulte os POI (pontos de interesse), crie e faça a gestão de percursos GPX, utilize linhas de contorno e informações de altitude, possibilidade de escolher entre os modos de condução, bicicleta ou pedestre, edição de dados para o OSM e muito mais.

OsmAND+ é a versão paga da aplicação. Se a comprar, está a ajudar o projeto apoiando o desenvolvimento de novas funcionalidades bem como a ter acesso às versões mais recentes.

Algumas das suas funcionalidades: Navegação • Modo online (rápido) e offline (sem tarifas de acesso à Internet) • Navegação por voz (vozes gravadas ou sintetizadas) • Exibição do nome das ruas e tempo estimado da viagem • Suporte a pontos de interesse existentes no seu itinerário • Reencaminhamento imediato assim que se desviar da sua rota • Pesquisa de locais por endereço, tipo (restaurante, museu, estação de serviço, hotel...) ou por coordenadas geográficas

Todas as strings combinadas não podem exceder os 4000 caracteres

Todas as strings de descrição não podem exceder os 4000 caracteres

Todas as strings de descrição não podem exceder os 4000 caracteres

Características para bicicletas e pedestres • ver caminhos a pé, caminhadas e bicicletas, ideal para actividades ao ar livre • modos de encaminhamento e visualização especiais para bicicletas e pedestres • paragens de transportes públicos opcionais (autocarro, eléctrico, comboio) incluindo nomes de linhas • gravação de viagem opcional para um ficheiro GPX local ou serviço on-line • visualização de velocidade e altitude opcionais • exibição de linhas de contorno e sombreamento de morro (via plugin adicional)

Contribua diretamente para OSM • Relatar bugs de dados • Enviar faixas GPX para OSM diretamente do aplicativo • Adicionar PIS e enviá-los diretamente para OSM (ou mais tarde se estiver offline) • Gravação de viagem opcional também no modo de fundo (enquanto o dispositivo está no modo de suspensão) OsmAnd é de código aberto e ativo mente desenvolvida. Todos podem contribuir para o aplicativo, relatando bugs, melhorando traduções ou codificando novos recursos. O projeto está em um estado muito animado de melhoria por todas formas de desenvolvimento e interação do usuário. O andamento do projeto também depende de contribuições financeiras para financiar a codificação e testes de novas funcionalidades. Cobertura aproximada do mapa e qualidade: • Europa Ocidental: * * * * • Europa Oriental: * * * • Rússia: * * * • America do Norte: * * * • Ámérica do Sul: * * • Ásia: * * • Japão & Coreia: * * * • Médio Oriente: * * • África: * * • continente Antárctico Do Afeganistão ao Zimbábue, da Austrália aos EUA. Argentina, Brasil, Canadá, França, Alemanha, México, Reino Unido, Espanha,...

Anticaracterísticas: * NonFreeAssets - arte e layouts estão sob uma licença não-comercial.
